//----------------------------------------------------------------------
// Gold Parser engine.
// See more details on http://www.devincook.com/goldparser/
// 
// Original code is written in VB by Devin Cook (GOLDParser@DevinCook.com)
//
// This translation is done by Vladimir Morozov (vmoroz@hotmail.com)
// 
// The translation is based on the other engine translations:
// Delphi engine by Alexandre Rai (riccio@gmx.at)
// C# engine by Marcus Klimstra (klimstra@home.nl)
//----------------------------------------------------------------------

using System;

namespace GoldParser
{
	/// <summary>
	/// Pull parser which uses Grammar table to parser input stream.
	/// </summary>
	public sealed class Parser
	{
		#region Fields
		
		private Grammar _mGrammar;          
		private bool    _mTrimReductions;   

//		private TextReader m_textReader;       // Data to parse.
		private readonly char[]     _mBuffer;           // Buffer to keep current characters.
//		private int        m_bufferSize;       // Size of the buffer.
//		private int        m_bufferStartIndex; // Absolute position of buffered first character. 
		private int        _mCharIndex;        // Index of character in the buffer.
		private int        _mPreserveChars;    // Number of characters to preserve when buffer is refilled.
//		private int        m_lineStart;        // Relative position of line start to the buffer beginning.
//		private int        m_lineLength;       // Length of current source line.
//		private int        m_lineNumber = 1;   // Current line number.
		private int        _mCommentLevel;     // Keeps stack level for embedded comments
//		private StringBuilder m_commentText;   // Current comment text.

		private Token   _mToken;            // Current token
		private readonly Token[] _mInputTokens;      // Stack of input tokens.
		private int     _mInputTokenCount;  // How many tokens in the input.
		
		private LrStackItem[] _mLrStack;        // Stack of LR states used for LR parsing.
		private int           _mLrStackIndex;   // Index of current LR state in the LR parsing stack. 
		private LRState       _mLrState;        // Current LR state.
		private int           _mReductionCount; // Number of items in reduction. It is Undefined if no reducton available. 

		private const char EndOfString = '\u0000';     // Designates last string terminator.
		private const int  MinimumInputTokenCount = 2; // Minimum input token stack size.
		private const int  MinimumLrStackSize = 32;   // Minimum size of reduction stack.
		private const int  Undefined = -1;             // Used for undefined int values. 
		
		#endregion

		#region Constructors

		public Parser(string formula, Grammar grammar)
		{
			_mBuffer = formula.ToCharArray();

			_mInputTokens = new Token[MinimumInputTokenCount];
			_mLrStack = new LrStackItem[MinimumLrStackSize];

			_mGrammar = grammar;

			// Put grammar start symbol into LR parsing stack.
			_mLrState = _mGrammar.InitialLRState;
			LrStackItem start = new LrStackItem();
			start.MToken.MSymbol = _mGrammar.StartSymbol;
			start.MState = _mLrState;
			_mLrStack[_mLrStackIndex] = start;

			_mReductionCount = Undefined; // there are no reductions yet.

		}

		#endregion

		#region Parser general properties

		/// <summary>
		/// Gets or sets flag to trim reductions.
		/// </summary>
		public bool TrimReductions
		{
			get { return _mTrimReductions; }
			set { _mTrimReductions = value; }
		}

		#endregion

		#region Tokenizer properties and methods

		/// <summary>
		/// Gets or sets current token symbol.
		/// </summary>
		public Symbol TokenSymbol
		{
			get { return _mToken.MSymbol; }
			set { _mToken.MSymbol = value; }
		}

		/// <summary>
		/// Gets or sets current token text.
		/// </summary>
		public string TokenText 
		{
			get 
			{ 
				if (_mToken.MText == null)
				{
					if (_mToken.MLength > 0)
					{
						//m_token.m_text = new String(m_buffer, m_token.m_start - m_bufferStartIndex, m_token.m_length);
						_mToken.MText = new String(_mBuffer, _mToken.MStart, _mToken.MLength);
					}
					else
					{
						_mToken.MText = string.Empty;
					}
				}
				return _mToken.MText; 
			}
			set { _mToken.MText = value; }
		}

		/// <summary>
		/// Gets or sets token syntax object associated with the current token or reduction.
		/// </summary>
		public object TokenSyntaxNode 
		{
			get 
			{ 
				if (_mReductionCount == Undefined)
					return _mToken.MSyntaxNode; 
				return _mLrStack[_mLrStackIndex].MToken.MSyntaxNode;
			}
			set 
			{ 
				if (_mReductionCount == Undefined)
					_mToken.MSyntaxNode = value;
				else
					_mLrStack[_mLrStackIndex].MToken.MSyntaxNode = value;
			}
		}

		/// <summary>
		/// Reads next token from the input stream.
		/// </summary>
		/// <returns>Token symbol which was read.</returns>
		public Symbol ReadToken()
		{
			_mToken.MText = null;
//			m_token.m_start = m_charIndex + m_bufferStartIndex;
			_mToken.MStart = _mCharIndex;
			int lookahead   = _mCharIndex;  // Next look ahead char in the input
			int tokenLength = 0;       
			Symbol tokenSymbol = null;
			
			char ch = _mBuffer[lookahead];
			if (ch == EndOfString)
			{
				_mToken.MSymbol = _mGrammar.m_endSymbol;
				_mToken.MLength = 0;
				return _mToken.MSymbol;
			}
			DfaState dfaState = _mGrammar.m_dfaInitialState;
			while (true)
			{
				dfaState = dfaState.m_transitionVector[ch] as DfaState;
				if (dfaState != null)
				{
					// This code checks whether the target state accepts a token. If so, it sets the
					// appropiate variables so when the algorithm in done, it can return the proper
					// token and number of characters.
					lookahead++;
					if (dfaState.m_acceptSymbol != null)
					{
						tokenSymbol = dfaState.m_acceptSymbol;
						tokenLength = lookahead - _mCharIndex;
					}
					ch = _mBuffer[lookahead];
					if (ch == EndOfString)
					{
						_mPreserveChars = lookahead - _mCharIndex;
						// Found end of of stream
						lookahead = _mCharIndex + _mPreserveChars;
						_mPreserveChars = 0;
					}
				}
				else
				{
					if (tokenSymbol != null)
					{
						_mToken.MSymbol = tokenSymbol;
						_mToken.MLength = tokenLength;
						_mCharIndex += tokenLength;
					}
					else
					{
						//Tokenizer cannot recognize symbol
						_mToken.MSymbol = _mGrammar.m_errorSymbol;
						_mToken.MLength = 1;
						_mCharIndex++;
					}        
					break;
				}
			}
			return _mToken.MSymbol;
		}

		/// <summary>
		/// Removes current token and pops next token from the input stack.
		/// </summary>
		private void DiscardInputToken()
		{
			if (_mInputTokenCount > 0)
			{
				_mToken = _mInputTokens[--_mInputTokenCount];
			}
			else
			{
				_mToken.MSymbol = null;
				_mToken.MText = null;
			}
		}

		#endregion

		#region LR parser properties and methods

		/// <summary>
		/// Gets current LR state.
		/// </summary>
		public LRState CurrentLrState
		{
			get { return _mLrState; }
		}

		/// <summary>
		/// Gets current reduction syntax rule.
		/// </summary>
		public Rule ReductionRule 
		{
			get { return _mLrStack[_mLrStackIndex].MRule; }
		}

		/// <summary>
		/// Gets reduction item syntax object by its index.
		/// </summary>
		/// <param name="index">Index of reduction item.</param>
		/// <returns>Syntax object attached to reduction item.</returns>
		public object GetReductionSyntaxNode(int index)
		{
			if (index < 0 || index >= _mReductionCount)
			{
				throw new IndexOutOfRangeException();
			}
			return _mLrStack[_mLrStackIndex - _mReductionCount + index].MToken.MSyntaxNode;
		}

		/// <summary>
		/// Executes next step of parser and returns parser state.
		/// </summary>
		/// <returns>Parser current state.</returns>
		public ParseMessage Parse()
		{
			while (true)
			{
				if (_mToken.MSymbol == null)
				{
					//We must read a token
					Symbol readTokenSymbol = ReadToken();
					SymbolType symbolType = readTokenSymbol.m_symbolType;					
					if (_mCommentLevel == 0 
						&& symbolType != SymbolType.CommentLine
						&& symbolType != SymbolType.CommentStart
						&& symbolType != SymbolType.WhiteSpace) 
					{
						return ParseMessage.TokenRead;
					}
				}
				else
				{
					//==== Normal parse mode - we have a token and we are not in comment mode
					switch (_mToken.MSymbol.m_symbolType)
					{
						case SymbolType.WhiteSpace:
							DiscardInputToken();  // Discard Whitespace
							break;

						case SymbolType.CommentStart:
							_mCommentLevel = 1; // Switch to block comment mode.
							return ParseMessage.CommentBlockRead;

						case SymbolType.CommentLine:
							return ParseMessage.CommentLineRead;
								
						case SymbolType.Error:
							return ParseMessage.LexicalError;
					
						default:
							//Finally, we can parse the token
							TokenParseResult parseResult = ParseToken();
						switch (parseResult)
						{
							case TokenParseResult.Accept:
								return ParseMessage.Accept;

							case TokenParseResult.InternalError:
								return ParseMessage.InternalError;

							case TokenParseResult.ReduceNormal:
								return ParseMessage.Reduction;

							case TokenParseResult.Shift: 
								//A simple shift, we must continue
								DiscardInputToken(); // Okay, remove the top token, it is on the stack
								break;

							case TokenParseResult.SyntaxError:
								return ParseMessage.SyntaxError;

							default:
								//Do nothing
								break;
						}
							break;
					}
				}
			}
		}

		private TokenParseResult ParseToken()
		{
			LRStateAction stateAction = _mLrState.m_transitionVector[_mToken.MSymbol.m_index];
			if (stateAction != null)
			{
				//Work - shift or reduce
				if (_mReductionCount > 0)
				{
					int newIndex = _mLrStackIndex - _mReductionCount;
					_mLrStack[newIndex] = _mLrStack[_mLrStackIndex];
					_mLrStackIndex = newIndex;
				}
				_mReductionCount = Undefined;
				switch (stateAction.Action)
				{
					case LRAction.Accept:
						_mReductionCount = 0;
						return TokenParseResult.Accept;
	
					case LRAction.Shift:
						_mLrState = _mGrammar.m_lrStateTable[stateAction.m_value];
						LrStackItem nextToken = new LrStackItem();
						nextToken.MToken = _mToken;
						nextToken.MState = _mLrState;
						if (_mLrStack.Length == ++_mLrStackIndex)
						{
							LrStackItem[] largerMLrStack = new LrStackItem[_mLrStack.Length + MinimumLrStackSize];
							Array.Copy(_mLrStack, largerMLrStack, _mLrStack.Length);
							_mLrStack = largerMLrStack;
						}
						_mLrStack[_mLrStackIndex] = nextToken;
						return TokenParseResult.Shift;

					case LRAction.Reduce:
						//Produce a reduction - remove as many tokens as members in the rule & push a nonterminal token
						int ruleIndex = stateAction.m_value;
						Rule currentRule = _mGrammar.m_ruleTable[ruleIndex];

						//======== Create Reduction
						LrStackItem head;
						TokenParseResult parseResult;
						LRState nextState;
						if (_mTrimReductions && currentRule.m_hasOneNonTerminal) 
						{
							//The current rule only consists of a single nonterminal and can be trimmed from the
							//parse tree. Usually we create a new Reduction, assign it to the Data property
							//of Head and push it on the stack. However, in this case, the Data property of the
							//Head will be assigned the Data property of the reduced token (i.e. the only one
							//on the stack).
							//In this case, to save code, the value popped of the stack is changed into the head.
							head = _mLrStack[_mLrStackIndex];
							head.MToken.MSymbol = currentRule.m_nonTerminal;
							head.MToken.MText = null;
							parseResult = TokenParseResult.ReduceEliminated;
							//========== Goto
							nextState = _mLrStack[_mLrStackIndex - 1].MState;
						}
						else
						{
							//Build a Reduction
							head = new LrStackItem();
							head.MRule = currentRule;
							head.MToken.MSymbol = currentRule.m_nonTerminal;
							head.MToken.MText = null;
							_mReductionCount = currentRule.m_symbols.Length;
							parseResult = TokenParseResult.ReduceNormal;
							//========== Goto
							nextState = _mLrStack[_mLrStackIndex - _mReductionCount].MState;
						}

						//========= If nextAction is null here, then we have an Internal Table Error!!!!
						LRStateAction nextAction = nextState.m_transitionVector[currentRule.m_nonTerminal.m_index];
						if (nextAction != null)
						{
							_mLrState = _mGrammar.m_lrStateTable[nextAction.m_value];
							head.MState = _mLrState;
							if (parseResult == TokenParseResult.ReduceNormal)
							{
								if (_mLrStack.Length == ++_mLrStackIndex)
								{
									LrStackItem[] largerMLrStack = new LrStackItem[_mLrStack.Length 
										+ MinimumLrStackSize];
									Array.Copy(_mLrStack, largerMLrStack, _mLrStack.Length);
									_mLrStack = largerMLrStack;
								}
								_mLrStack[_mLrStackIndex] = head;
							}
							else
							{
								_mLrStack[_mLrStackIndex] = head;
							}
							return parseResult;
						}
						else
						{
							return TokenParseResult.InternalError;
						}
				}
			}
			return TokenParseResult.SyntaxError;
		}

		#endregion

		#region TokenParseResult enumeration

		/// <summary>
		/// Result of parsing token.
		/// </summary>
		private enum TokenParseResult
		{
			Empty            = 0,
			Accept           = 1,
			Shift            = 2,
			ReduceNormal     = 3,
			ReduceEliminated = 4,
			SyntaxError      = 5,
			InternalError    = 6
		}

		#endregion

		#region Token struct

		/// <summary>
		/// Represents data about current token.
		/// </summary>
		private struct Token
		{
			internal Symbol MSymbol;     // Token symbol.
			internal string MText;       // Token text.
			internal int MStart;         // Token start stream start.
			internal int MLength;        // Token length.
			internal object MSyntaxNode; // Syntax node which can be attached to the token.
		}

		#endregion

		#region LRStackItem struct

		/// <summary>
		/// Represents item in the LR parsing stack.
		/// </summary>
		private struct LrStackItem
		{
			internal Token MToken;   // Token in the LR stack item.
			internal LRState MState; // LR state associated with the item.
			internal Rule MRule;     // Reference to a grammar rule if the item contains non-terminal.
		}

		#endregion
	}
}
