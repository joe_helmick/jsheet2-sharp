#region Copyright

//----------------------------------------------------------------------
// Gold Parser engine.
// See more details on http://www.devincook.com/goldparser/
// 
// Original code is written in VB by Devin Cook (GOLDParser@DevinCook.com)
//
// This translation is done by Vladimir Morozov (vmoroz@hotmail.com)
// 
// The translation is based on the other engine translations:
// Delphi engine by Alexandre Rai (riccio@gmx.at)
// C# engine by Marcus Klimstra (klimstra@home.nl)
//----------------------------------------------------------------------

#endregion

#region Using directives

using System;
using System.Text;

#endregion

namespace GoldParser
{
	public class Rule
	{
		private int m_index;
		internal Symbol m_nonTerminal;
		internal Symbol[] m_symbols;
		internal bool m_hasOneNonTerminal;

		public Rule(int index, Symbol nonTerminal, Symbol[] symbols)
		{
			m_index = index;
			m_nonTerminal = nonTerminal;
			m_symbols = symbols;
			m_hasOneNonTerminal = (symbols.Length == 1) 
				&& (symbols[0].SymbolType == SymbolType.NonTerminal); 
		}

		/// <summary>
		/// Gets index of the rule in the rule table.
		/// </summary>
		public int Index 
		{
			get { return m_index; }
		}

		/// <summary>
		/// Gets symbol by its index.
		/// </summary>
		public Symbol this[int index] 
		{
			get { return m_symbols[index]; }
		}

	}
}
