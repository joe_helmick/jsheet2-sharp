#region Copyright

//----------------------------------------------------------------------
// Gold Parser engine.
// See more details on http://www.devincook.com/goldparser/
// 
// Original code is written in VB by Devin Cook (GOLDParser@DevinCook.com)
//
// This translation is done by Vladimir Morozov (vmoroz@hotmail.com)
// 
// The translation is based on the other engine translations:
// Delphi engine by Alexandre Rai (riccio@gmx.at)
// C# engine by Marcus Klimstra (klimstra@home.nl)
//----------------------------------------------------------------------

#endregion

#region Using directives

using System;
using System.Text;

#endregion

namespace GoldParser
{
	public class Symbol
	{
		internal int       m_index;      // symbol index in symbol table
		internal SymbolType m_symbolType; // type of the symbol

		public Symbol(int index, string name, SymbolType symbolType)
		{
			m_index = index;
			m_symbolType = symbolType;
		}

		public int Index 
		{
			get { return m_index; }
		}

		public SymbolType SymbolType 
		{
			get { return m_symbolType; }
		}

	}
}
