﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoldParser
{

	public enum LRAction
	{
		None = 0,
		Shift = 1,
		Reduce = 2,
		Goto = 3,
		Accept = 4,
		Error = 5
	}

	public enum ParseMessage
	{
		Empty = 0,
		TokenRead = 1,
		Reduction = 2,
		Accept = 3,
		NotLoadedError = 4,
		LexicalError = 5,
		SyntaxError = 6,
		CommentError = 7,
		InternalError = 8,
		CommentBlockRead = 9,
		CommentLineRead = 10,
	}

	public enum SymbolType
	{
		NonTerminal = 0,
		Terminal = 1,
		WhiteSpace = 2,
		End = 3,
		CommentStart = 4,
		CommentEnd = 5,
		CommentLine = 6,
		Error = 7
	}
}
