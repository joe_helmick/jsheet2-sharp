#region Copyright

//----------------------------------------------------------------------
// Gold Parser engine.
// See more details on http://www.devincook.com/goldparser/
// 
// Original code is written in VB by Devin Cook (GOLDParser@DevinCook.com)
//
// This translation is done by Vladimir Morozov (vmoroz@hotmail.com)
// 
// The translation is based on the other engine translations:
// Delphi engine by Alexandre Rai (riccio@gmx.at)
// C# engine by Marcus Klimstra (klimstra@home.nl)
//----------------------------------------------------------------------

#endregion

#region Using directives

using System;

#endregion

namespace GoldParser
{
	public class LRState
	{
		private int m_index;
		private LRStateAction[] m_actions;
		internal LRStateAction[] m_transitionVector;

		public LRState(int index, LRStateAction[] actions, LRStateAction[] transitionVector)
		{
			m_index = index;
			m_actions = actions;
			m_transitionVector = transitionVector;
		}
	}
}
