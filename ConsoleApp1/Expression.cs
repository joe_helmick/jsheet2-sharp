﻿using System;

namespace ConsoleApp1
{
	public class SyntaxNode
	{
		public SyntaxNode(Context context) => Context = context;
		public Context Context { get; }
	}

	public class Expression : SyntaxNode
	{
		public Expression(Context context)
			: base(context)
		{
		}
		public virtual object Value => null;
	}

	public class BinaryExpression : Expression
	{
		private readonly Expression _leftOperand;
		private readonly string _operator;
		private readonly Expression _rightOperand;

		public BinaryExpression(Context context,
			Expression leftOperand, string op, Expression rightOperand)
			: base(context)
		{
			_leftOperand = leftOperand;
			_operator = op;
			_rightOperand = rightOperand;
		}

		public override object Value
		{
			get
			{
				int lValue = Convert.ToInt32(_leftOperand.Value);
				int rValue = Convert.ToInt32(_rightOperand.Value);
				switch (_operator.ToUpper())
				{
					case "+":
						return lValue + rValue;
					case "-":
						return lValue - rValue;
					case "*":
						return lValue * rValue;
					case "/":
						return lValue / rValue;
				}
				Sc.ErrorText = "expression error";
				return null;
			}
		}

		public override string ToString()
		{
			string s = _leftOperand + " " + _operator + " " + _rightOperand;
			return s;
		}
	}


	public class UnaryMinusExpression : Expression
	{
		private readonly Expression _operand;
		public UnaryMinusExpression(Context context, Expression operand)
			: base(context)
		{
			_operand = operand;
		}

		public override object Value => -Convert.ToInt32(_operand.Value);

		public override string ToString()
		{
			string s = " - " + _operand;
			return s;
		}

	}


	public class Number : Expression
	{
		private readonly int _mValue;
		public Number(Context context, int value)
			: base(context)
		{
			_mValue = value;
		}

		public override object Value => _mValue;
	}


	public class Transfer : Expression
	{
		private readonly Expression _operand;
		public Transfer(Context context, Expression operand)
			: base(context)
		{
			_operand = operand;
		}

		public override object Value => Convert.ToInt32(_operand.Value);

		public override string ToString()
		{
			string s = " transfer " + _operand;
			return s;
		}

	}


}
