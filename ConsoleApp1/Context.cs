﻿using System;
using System.Collections.Generic;
using ConsoleApp1.Engine;

namespace ConsoleApp1
{

	public class Context
	{
		public string CurrentCellReference { get; set; }
		private readonly Parser _parser;
		public Context(Parser parser)
		{
			_parser = parser;
		}

		public string GetTokenText()
		{
			switch (_parser.TokenSymbol.Index)
			{
				case (int) SymbolConstants.Eof:
					return null;
				case (int)SymbolConstants.Error:
					return "ERROR";
				case (int) SymbolConstants.Minus:
				case (int) SymbolConstants.LParen:
				case (int) SymbolConstants.RParen:
				case (int) SymbolConstants.Mult:
				case (int) SymbolConstants.Div:
				case (int) SymbolConstants.Plus:
				case (int) SymbolConstants.CellRef:
				case (int) SymbolConstants.Number:
					return _parser.TokenText;
				default:
					Sc.ErrorText = "GetTokenText()" + _parser.TokenSymbol.Index;
					return null;
			}
		}

		public SyntaxNode GetSyntaxNode()
		{
			switch (_parser.ReductionRule.Index)
			{
				case (int)RuleConstants.Formula: // handle empty input
					return new Number(this, 0);
				case (int) RuleConstants.AddAddPlusMult:
				case (int) RuleConstants.AddAddMinusMult:
				case (int) RuleConstants.MultMultTimesNeg:
				case (int) RuleConstants.MultMultDivNeg:
				{
					return new BinaryExpression(this, Expression(0), Token(1), Expression(2));
				}

				case (int)RuleConstants.NegMinusValue:
				{
					return new UnaryMinusExpression(this, Expression(1));
				}

				case (int)RuleConstants.ValueCellRef:
				{
					if (Sc.FindingDependencies)
					{
						if (Sc.Dependencies.ContainsKey(CurrentCellReference))
							Sc.Dependencies[CurrentCellReference].Add(Token(0));
						else
						{
							Sc.Dependencies.Add(CurrentCellReference, new List<string>());
							Sc.Dependencies[CurrentCellReference].Add(Token(0));
						}
					}
					else
					{
						string cr = Token(0);
						int x = Sc.CellRefs[cr];
						Number n = new Number(this, x);
						return n;
					}
					Number ndef = new Number(this, 0);
					return ndef;
				}

				case (int) RuleConstants.ValueNumber:
				{
					Number n = new Number(this, Convert.ToInt32(Token(0)));
					return n;
				}

				case (int) RuleConstants.ValueParenExp:
				{
					return Expression(1);
				}

				/*
				 The following cases are for rules that have exactly one non-terminal and nothing
					else on the right-hand side of the rule.  These would be skipped altogether 
					if Parser.TrimReductions is turned on, but in order to skip that somewhat complicated
					logic, I coded these up to simply "transfer" to the next reduction when 
					 encountered.  Now the code runs to completion, albeit with more reductions.  So the 
					tradeoff is speed vs code quantity and complexity.
				*/
				case (int)RuleConstants.AddMult:
				case (int)RuleConstants.MultNeg:
				case (int)RuleConstants.NegValue:
				case (int)RuleConstants.FormulaAdd:
				{
					return new Transfer(this, Expression(0));
				}
				default:
					return null;
			}

		}

		private Expression Expression(int index)
		{
			object grsn = _parser.GetReductionSyntaxNode(index);
			//Sc.ConWriteLine(CONMODE_EXPRESSION, "Context.Expression() " + grsn.ToString() + " " + index);
			return (Expression) grsn;
		}

		private string Token(int index)
		{
			object t = _parser.GetReductionSyntaxNode(index);
			//Sc.ConWriteLine(CONMODE_TOKEN, "Context.Token() " + t.ToString() + " " + index);
			return (string) t;
		}

	}
}
