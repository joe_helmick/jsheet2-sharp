﻿//#define STOP_AFTER_PARSE_1
//#define COLOR_SHOW

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using ConsoleApp1.Engine;


namespace ConsoleApp1
{
	internal class Program
	{
		// COLOR CONSTANTS
		private const int CONMODE_PROGRAM = 255;
		private const int CONMODE_PROGRAM_VALUE = 9;

		#region CONSOLE COLOR APIS
		// Windows imports to support extended console color modes.
		[DllImport("kernel32.dll", SetLastError = true)]
		public static extern bool SetConsoleMode(IntPtr hConsoleHandle, int mode);
		[DllImport("kernel32.dll", SetLastError = true)]
		public static extern bool GetConsoleMode(IntPtr handle, out int mode);
		[DllImport("kernel32.dll", SetLastError = true)]
		public static extern IntPtr GetStdHandle(int handle);
		#endregion

		// Create a parser and a context.
		private static Parser _parser;
		private static Context _context;

#if COLOR_SHOW
		// A color previewer.
		private static void ColorShow()
		{
			Sc.ConWriteLine(255, "16 BASICS");
			for (int c = 0; c < 16; c++)
			{
				Sc.ConWrite(c, "TEXT" + c.ToString("D3") + " ");
				if (c == 7)
					Sc.ConWrite(c, "\r\n");

			}
			Sc.ConWrite(0, "\r\n");

			Sc.ConWriteLine(255, "\r\n216 PALETTE 6x6x6");
			{
				int rowCount = 0;
				int colCount = 0;
				for (int c = 16; c <= 231; c++)
				{
					Sc.ConWrite(c, "TEXT" + c.ToString("D3") + " ");
					colCount++;
					if (colCount == 6)
					{
						Sc.ConWrite(c, "\r\n");
						colCount = 0;
						rowCount++;
					}

					if (rowCount == 6)
					{
						Sc.ConWrite(c, "\r\n");
						rowCount = 0;
					}
				}
			}

			Sc.ConWriteLine(255, "24 GREYS");
			{
				int rowCount = 0;
				for (int c = 232; c <= 255; c++)
				{
					Sc.ConWrite(c, "TEXT" + c.ToString("D3") + " ");
					rowCount++;
					if (rowCount == 6)
					{
						Sc.ConWrite(c, "\r\n");
						rowCount = 0;
					}
				}

			}
			Sc.ConWrite(0, "\r\n");


		}
#endif       
		static void Main()
		{
			// Ensure that the console can print UTF-8 characters (font must support this also)
			Console.OutputEncoding = Encoding.UTF8;
			// Initialize the console color stuff.
			var handle = GetStdHandle(-11);
			int mode;
			GetConsoleMode(handle, out mode);
			SetConsoleMode(handle, mode | 0x4);

#if COLOR_SHOW
			ColorShow();
#endif

			// Add formulas as if they'd been entered in the UI.
			//			Sc.FormulasList.Add(new KeyValuePair<string, string>("A1", "1\u0000"));

			Sc.FormulasList.Add(new KeyValuePair<string, string>("D1", "(2 + 3 * 4) * 5\u0000"));
			Sc.FormulasList.Add(new KeyValuePair<string, string>("B3", "(7 * 3)\u0000"));
			Sc.FormulasList.Add(new KeyValuePair<string, string>("B2", "B3 * 2\u0000"));
			Sc.FormulasList.Add(new KeyValuePair<string, string>("B4", "B3 / 2\u0000"));

			//Sc.FormulasList.Add(new KeyValuePair<string, string>("C7", "C8/0\u0000"));
			//Sc.FormulasList.Add(new KeyValuePair<string, string>("C1", "A3+B4\u0000"));
			//Sc.FormulasList.Add(new KeyValuePair<string, string>("B4", "A2*2+B5\u0000"));
			//Sc.FormulasList.Add(new KeyValuePair<string, string>("A3", "-A111+A2+5*453/3\u0000"));
			//Sc.FormulasList.Add(new KeyValuePair<string, string>("A111", "42\u0000"));
			//Sc.FormulasList.Add(new KeyValuePair<string, string>("B5", "32\u0000"));
			//Sc.FormulasList.Add(new KeyValuePair<string, string>("A2", "A111-1\u0000"));

			// Read the grammar file -- CGT FORMAT!
			using (Stream cgtStream = File.OpenRead(Sc.GRAMMAR_FILE_NAME))
			{
				BinaryReader cgtReader = new BinaryReader(cgtStream);
				Sc.Grammar = new Grammar(cgtReader);
			}

			// Start the program.
			Sc.ConWriteLine(CONMODE_PROGRAM, "START");

			// First just print the formulas for reference.
			Sc.ConWriteLine(CONMODE_PROGRAM, "Formulas Display:");
			foreach (var input in Sc.FormulasList)
				Sc.ConWriteLine(CONMODE_PROGRAM_VALUE, input.Key + '=' + input.Value);

			Sc.ConWriteLine(CONMODE_PROGRAM, "----------------------------------------------------------");

			// Now, parse each formula and build dependencies list while
			// doing it, so that the topological sort can run later if all formulas
			// parse cleanly.
			Sc.ConWriteLine(CONMODE_PROGRAM, "Formulas:");
			Sc.FindingDependencies = true;
			foreach (var input in Sc.FormulasList)
			{
				Sc.ThisCellReference = input.Key;
				Sc.ThisFormula = input.Value;
				Sc.ConWriteLine(CONMODE_PROGRAM_VALUE, "\r\nProgram.Formula: " + Sc.ThisCellReference + "=" + Sc.ThisFormula);
				// Reset artificial counter each time new formula is parsed.
				Sc.StackCounter = 0;
				Sc.ParseSuccess = DoParse(Sc.ThisFormula);
				if (Sc.ParseSuccess)
				{
					// Put a placeholder zero value in for this cell until it can be evaluated.
					Sc.CellRefs.Add(Sc.ThisCellReference, 0);
					// Add this Cell to the dependencies list with empty list of dependencies, if it has
					// no CellRefs in it so that it will be processed by the topological sort routine.
					if (!Sc.Dependencies.ContainsKey(Sc.ThisCellReference))
						Sc.Dependencies.Add(Sc.ThisCellReference, new List<string>());
				}
				else
				{
					Sc.UnrecoverableErrorFound = true;
					goto labelError;
				}
			}

			// Comment below statement to stop after syntax parsing, and not evaluate.
			//goto done;

			Sc.ConWriteLine(CONMODE_PROGRAM, "----------------------------------------------------------");
			Sc.ConWriteLine(CONMODE_PROGRAM, "\r\nProgram.Dependencies:");
			foreach (var dep in Sc.Dependencies)
			{
				Sc.ConWrite(CONMODE_PROGRAM_VALUE, dep.Key + " -> ");
				foreach (string d2 in dep.Value.ToArray())
					Sc.ConWrite(CONMODE_PROGRAM_VALUE, d2 + " ");
				Sc.ConWriteLine(CONMODE_PROGRAM_VALUE, "");
			}

			Sc.ConWriteLine(CONMODE_PROGRAM, "----------------------------------------------------------");
			// Do topological sort of formulas based on dependencies.
			Sc.ConWriteLine(CONMODE_PROGRAM, "\r\nProgram.Topological Sort Results:");
			TopoSort();
			if (Sc.UnrecoverableErrorFound)
				goto labelError;
			foreach (string cellref in Sc.TopoOrdered)
				Sc.ConWriteLine(CONMODE_PROGRAM_VALUE, cellref);

			Sc.ConWriteLine(CONMODE_PROGRAM, "----------------------------------------------------------");
			// Find #REF errors, where formula for cell reference is null.
			Sc.ConWriteLine(CONMODE_PROGRAM, "\r\nProgram.Checking for #REF errors");
			foreach (string cellref in Sc.TopoOrdered)
			{
				Sc.ThisCellReference = cellref;
				Sc.ThisFormula = FindFormula(Sc.ThisCellReference);
				if (string.IsNullOrEmpty(Sc.ThisFormula))
				{
					Sc.ErrorText = "#REF " + Sc.ThisCellReference;
					Sc.UnrecoverableErrorFound = true;
					goto labelError;
				}
			}
			Sc.ConWriteLine(CONMODE_PROGRAM_VALUE, "-- no #REF errors");

			Sc.ConWriteLine(CONMODE_PROGRAM, "----------------------------------------------------------");
			// Finally do all parsing and evaluating in the order determined by topological sort.
			Sc.ConWriteLine(CONMODE_PROGRAM, "Program.Computations:\r\n");
			Sc.FindingDependencies = false;
			foreach (string cellref in Sc.TopoOrdered)
			{
				Sc.ThisCellReference = cellref;
				Sc.ThisFormula = FindFormula(Sc.ThisCellReference);
				Sc.ConWriteLine(CONMODE_PROGRAM_VALUE, "Formula=" + Sc.ThisCellReference + "=" + Sc.ThisFormula);
				// Reset artificial counter each time new formula is parsed.
				Sc.StackCounter = 0;
				Sc.ParseSuccess = DoParse(Sc.ThisFormula);
				if (Sc.ParseSuccess)
				{
					object o = Sc.Formula.Value;
					Sc.CellRefs[Sc.ThisCellReference] = (int)o;
					Sc.ConWriteLine(CONMODE_PROGRAM_VALUE, "COMPUTED VALUE = " + o);
					Console.WriteLine("\r\n\r\n\r\n");
				}
			}

labelError:
			Sc.ConWrite(CONMODE_PROGRAM, Sc.ErrorText);
			goto cleanup;
done:

cleanup:
			// Clear out everything for next iteration.
			Sc.Dependencies.Clear();
			Sc.CellRefs.Clear();
			Sc.UnrecoverableErrorFound = false;
			Sc.ErrorText = "";
			Sc.ConWriteLine(CONMODE_PROGRAM, "DONE");
			// Wait for keystroke to close window.
			Console.ReadKey();
		}

		private static bool DoParse(string formula)
		{
			_parser = new Parser(formula, Sc.Grammar) { TrimReductions = false };
			_context = new Context(_parser) {CurrentCellReference = Sc.ThisCellReference };
			while (true)
			{
				ParseMessage msg = _parser.Parse();

				switch (msg)
				{
					case ParseMessage.LexicalError:
						Sc.ErrorText = "formula error " + _parser.TokenText;
						return false;

					case ParseMessage.SyntaxError:
						Sc.ErrorText = "formula error " + _parser.TokenText;
						return false;

					case ParseMessage.Reduction:
						_parser.TokenSyntaxNode = _context.GetSyntaxNode();
						break;

					case ParseMessage.Accept:
						Sc.ConWriteLine(255, "\r\nProgram.DoParse() ACCEPT");
						Sc.Formula = (Expression)_parser.TokenSyntaxNode;
						return true;

					case ParseMessage.TokenRead:
						_parser.TokenSyntaxNode = _context.GetTokenText();
						string tokenName = Sc.Grammar.SymbolTexts[_parser.TokenSymbol.Index];
						Sc.ConWriteLine(64, "token " + _parser.TokenText + " type " + tokenName);
						break;
				}
			}
		}

		private static string FindFormula(string key)
		{
			foreach (var fi in Sc.FormulasList)
			{
				if (fi.Key == key)
					return fi.Value;
			}
			return null;
		}

		static void TopoSort()
		{
			Sc.NodeState = new Dictionary<string, int>();
			Sc.StartSet = new List<string>();
			Sc.TopoOrdered = new List<string>();

			foreach (var graphItem in Sc.Dependencies)
				Sc.StartSet.Add(graphItem.Key);

			while (Sc.StartSet.Count > 0)
			{
				int ndx = Sc.StartSet.Count - 1;
				string nextKey = Sc.StartSet[ndx];
				Sc.StartSet.RemoveAt(ndx);
				DepthFirstSearch(nextKey);
				if (Sc.UnrecoverableErrorFound)
					return;
			}
		}

		static void DepthFirstSearch(string nodeKey)
		{
			Sc.NodeState[nodeKey] = Sc.TOPOSORT_GRAY;

			if (Sc.Dependencies.ContainsKey(nodeKey) == false)
				goto label2;

			foreach (string k in Sc.Dependencies[nodeKey])
			{
				if (Sc.NodeState.ContainsKey(k) == false)
					goto label1;

				Sc.StateK = Sc.NodeState[k];

				if (Sc.StateK == Sc.TOPOSORT_GRAY)
				{
					Sc.ErrorText = "DFS ERROR: cycle with " + k + ", " + nodeKey;
					Sc.UnrecoverableErrorFound = true;
					return;
				}
				if (Sc.StateK == Sc.TOPOSORT_BLACK)
					continue;
				label1:
				if (Sc.StartSet.Contains(k))
					Sc.StartSet.Remove(k);
				DepthFirstSearch(k);
			}

			label2:
			Sc.TopoOrdered.Add(nodeKey);
			Sc.NodeState[nodeKey] = Sc.TOPOSORT_BLACK;
		}
	}
}
