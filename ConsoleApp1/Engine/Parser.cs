﻿//----------------------------------------------------------------------
// Gold Parser engine.
// See more details on http://www.devincook.com/goldparser/
// 
// Original code is written in VB by Devin Cook (GOLDParser@DevinCook.com)
//
// This translation is done by Vladimir Morozov (vmoroz@hotmail.com)
// 
// The translation is based on the other engine translations:
// Delphi engine by Alexandre Rai (riccio@gmx.at)
// C# engine by Marcus Klimstra (klimstra@home.nl)
//----------------------------------------------------------------------

//#define INCLUDE_TRIM_CODE
//#define PRINT_STACK

using System;
using System.Text;

namespace ConsoleApp1.Engine
{
	/// <summary>
	/// Pull parser which uses Grammar table to parser input stream.
	/// </summary>
	public sealed class Parser
	{
		// Color constants.
		private const int COLOR_SHIFT = 42;
		private const int COLOR_REDUCE = 137;

		// Constants.
		private const char END_OF_STRING = '\u0000';   // Designates last string terminator.
		private const int MINIMUM_LR_STACK_SIZE = 32;  // Minimum size of reduction stack.
		private const int UNDEFINED = -1;              // Used for undefined int values. 

		// Private fields.
		private readonly Grammar _mGrammar;            // Grammar loaded from CGT file.
		private Token _mToken;                         // Current token
		private readonly char[] _mBuffer;              // Buffer to keep current characters.
		private int _chPos;                            // Index of character in the buffer.
		private int _mPreserveChars;                   // Number of characters to preserve when buffer is refilled.
		private readonly LrStackItem[] _stack;         // Stack of LR states used for LR parsing.
		private int _stackIndex;                       // Index of current LR state in the LR parsing stack. 
		private int _rhsCount;                         // Number of items in reduction. It is UNDEFINED if no reduction available. 
		
		public Parser(string formula, Grammar grammar)
		{
			_mBuffer = formula.ToCharArray();
			_stack = new LrStackItem[MINIMUM_LR_STACK_SIZE];
			for (int k = 0; k < MINIMUM_LR_STACK_SIZE; k++) { LrStackItem item = -1; _stack[k] = item; }
			_mGrammar = grammar;
			CurrentLrState = _mGrammar.InitialLrState;

			// Create a grammar start symbol item.
			LrStackItem start = new LrStackItem();
			start.MToken.MSymbol = _mGrammar.StartSymbol;
			start.MState = CurrentLrState;
			start.Counter = ++Sc.StackCounter;

			// Put the start symbol onto the LR parsing stack.
			_stack[_stackIndex] = start;
			
			_rhsCount = UNDEFINED; // there are no reductions yet.
		}

#if PRINT_STACK
		public void PrintStack()
		{
			Sc.ConWriteLine(MODE_STACK_LIST, "STACK");
			for (int k = 0; k < MINIMUM_LR_STACK_SIZE; k++)
			{
				if (_stack[k].Counter >= 0)
				{
					Sc.ConWrite(170, k.ToString("D2") + " ");
					Sc.ConWriteLine(MODE_STACK_LIST, _stack[k].ToString());
				}
			}
			Sc.ConWriteLine(MODE_STACK_LIST, "");
		}
#endif

		public bool TrimReductions { get; set; }

		public Symbol TokenSymbol
		{
			get => _mToken.MSymbol;
			set => _mToken.MSymbol = value;
		}

		public string TokenText 
		{
			get 
			{
				if (_mToken.MText != null) return _mToken.MText;
				if (_mToken.MLength > 0)
				{
					//m_token.m_text = new String(m_buffer, m_token.m_start - m_bufferStartIndex, m_token.m_length);
					_mToken.MText = new String(_mBuffer, _mToken.MStart, _mToken.MLength);
				}
				else
					_mToken.MText = string.Empty;
				return _mToken.MText; 
			}
			set => _mToken.MText = value;
		}

		public object TokenSyntaxNode 
		{
			get 
			{ 
				if (_rhsCount == UNDEFINED)
					return _mToken.MSyntaxNode;
				return _stack[_stackIndex].MToken.MSyntaxNode;
			}
			set 
			{ 
				if (_rhsCount == UNDEFINED)
					_mToken.MSyntaxNode = value;
				else
					_stack[_stackIndex].MToken.MSyntaxNode = value;
			}
		}

		public Symbol ReadToken()
		{
			_mToken.MStart = _chPos;
			int lookahead   = _chPos;
			int tokenLength = 0;       
			Symbol tokenSymbol = null;
			// Look ahead
			char ch = _mBuffer[lookahead];
			// Handle end of string null character
			if (ch == END_OF_STRING)
			{
				_mToken.MText = "⬤";  // UTF-8 character to show EOF
				_mToken.MSymbol = _mGrammar.m_endSymbol;
				_mToken.MLength = 0;
				return _mToken.MSymbol;
			}
			DfaState dfaState = _mGrammar.m_dfaInitialState;
			while (true)
			{
				dfaState = dfaState.m_transitionVector[ch] as DfaState;
				if (dfaState != null)
				{
					// This code checks whether the target state accepts a token. If so, it sets the
					// appropriate variables so when the algorithm is done, it can return the proper
					// token and number of characters.
					lookahead++;
					if (dfaState.m_acceptSymbol != null)
					{
						tokenSymbol = dfaState.m_acceptSymbol;
						tokenLength = lookahead - _chPos;
					}
					ch = _mBuffer[lookahead];
					if (ch == END_OF_STRING)
					{
						_mPreserveChars = lookahead - _chPos;
						// Found end of of stream
						lookahead = _chPos + _mPreserveChars;
						_mPreserveChars = 0;
					}
				}

				if (dfaState == null)
				{
					if (tokenSymbol != null)
					{
						_mToken.MSymbol = tokenSymbol;
						_mToken.MLength = tokenLength;
						_chPos += tokenLength;
					}
					else
					{
						//Tokenizer cannot recognize symbol
						_mToken.MSymbol = _mGrammar.m_errorSymbol;
						_mToken.MLength = 1;
						_chPos++;
					}
					break;
				}
			} // while true
			return _mToken.MSymbol;
		}

		private void DiscardInputToken()
		{
			_mToken.MSymbol = null;
			_mToken.MText = null;
		}

		public LrState CurrentLrState { get; private set; }

		public Rule ReductionRule
		{
			get => _stack[_stackIndex].MRule;
			set => _stack[_stackIndex].MRule = value;
		}

		public object GetReductionSyntaxNode(int index)
		{
			if (index < 0 || index >= _rhsCount)
				throw new IndexOutOfRangeException();
			int k = _stackIndex - _rhsCount + index;
			object m = _stack[k].MToken.MSyntaxNode;
			return m;
		}

		public ParseMessage Parse()
		{
			while (true)
			{
				if (_mToken.MSymbol == null)
				{
					// We must read a token
					Symbol readTokenSymbol = ReadToken();
					SymbolType symbolType = readTokenSymbol.m_symbolType;
					if (symbolType != SymbolType.WhiteSpace)
						return ParseMessage.TokenRead;
				}
				else
				{
					switch (_mToken.MSymbol.m_symbolType)
					{
						case SymbolType.WhiteSpace:
							DiscardInputToken();
							break;

						case SymbolType.Error:
							return ParseMessage.LexicalError;

						default:
							TokenParseResult parseResult = ParseToken();
							switch (parseResult)
							{
								case TokenParseResult.Accept:
									#if PRINT_STACK
										Sc.ConWriteLine(MODE_ACCEPT, "ACCEPT");
										PrintStack();
									#endif
									return ParseMessage.Accept;

								case TokenParseResult.InternalError:
									return ParseMessage.InternalError;

								case TokenParseResult.ReduceNormal:
									#if PRINT_STACK
										Sc.ConWriteLine(COLOR_REDUCE, "* reduction rule " + ReductionRule.Index);
										PrintStack();
									#endif
									return ParseMessage.Reduction;

								case TokenParseResult.Shift:
									#if PRINT_STACK
										Sc.ConWriteLine(COLOR_SHIFT, " shift to state  " + CurrentLrState.m_index);
										PrintStack();
									#endif
									DiscardInputToken();
									break;

								case TokenParseResult.SyntaxError:
									return ParseMessage.SyntaxError;
							}
							break;
					}
				}
			}
		}

		private TokenParseResult ParseToken()
		{
			LrStateAction stateAction = CurrentLrState.m_transitionVector[_mToken.MSymbol.m_index];



			if (stateAction != null)
			{
				// This somehow shifts the current stack index based on the number of symbols on the 
				// right-hand side of a BNF rule.
				if (_rhsCount > 0)
				{
					int newIndex = _stackIndex - _rhsCount;
					_stack[newIndex] = _stack[_stackIndex];

					//∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎
					// Just show what's going on.
					#if PRINT_STACK
						Sc.ConWriteLine(173, "copy stack[" + _stackIndex + "] ==> stack[" + newIndex + "] rhsCount " + _rhsCount);
					#endif
					// Empty out higher indices
					for (int k = _stackIndex; k < MINIMUM_LR_STACK_SIZE; k++)
					{
						LrStackItem item = -1;
						_stack[k] = item;
					}
					//∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎
					_stackIndex = newIndex;
				}

				// This is important!
				_rhsCount = UNDEFINED;

				// Now do whatever action is required.
				switch (stateAction.Action)
				{
					case LrAction.Accept:
						_rhsCount = 0;
						return TokenParseResult.Accept;
	
					case LrAction.Shift:
						CurrentLrState = _mGrammar.m_lrStateTable[stateAction.m_value];
						LrStackItem nextToken = new LrStackItem();
						nextToken.Counter = ++Sc.StackCounter;
						nextToken.MToken = _mToken;
						nextToken.MState = CurrentLrState;
						_stackIndex++;
						if (_stack.Length == _stackIndex)
						{
							return TokenParseResult.StackError;
							//int newSize = stack.Length + MinimumLrStackSize;
							//LrStackItem[] largerMLrStack = new LrStackItem[newSize];
							//Array.Copy(stack, largerMLrStack, stack.Length);
							//stack = largerMLrStack;
						}
						_stack[_stackIndex] = nextToken;
						Sc.ConWriteLine(COLOR_SHIFT, "shift to state " + CurrentLrState.m_index);

						return TokenParseResult.Shift;

					case LrAction.Reduce:
						//Produce a reduction - remove as many tokens as members in the rule & push a nonterminal token
						int ruleIndex = stateAction.m_value;
						Rule currentRule = _mGrammar.m_ruleTable[ruleIndex];
						// JLH These are simply declared here, because these declarations will be used
						// one of two ways, expressed by the if() statement below.
						LrStackItem head;
						TokenParseResult parseResult;
						LrState nextState;
						#if INCLUDE_TRIM_CODE
							if (_mTrimReductions && currentRule.m_hasOneNonTerminal)
							{
								//The current rule only consists of a single nonterminal and can be trimmed from the
								//parse tree. Usually we create a new Reduction, assign it to the Data property
								//of Head and push it on the stack. However, in this case, the Data property of the
								//Head will be assigned the Data property of the reduced token (i.e. the only one
								//on the stack).
								//In this case, to save code, the value popped of the stack is changed into the head.
								head = stack[stackIndex];
								head.MToken.MSymbol = currentRule.m_nonTerminal;
								head.MToken.MText = null;
								parseResult = TokenParseResult.ReduceEliminated;
								//========== Goto
								nextState = stack[stackIndex - 1].MState;
							}
							else
							{
						#endif
							head = new LrStackItem();
							head.Counter = ++Sc.StackCounter;
							head.MRule = currentRule;
							head.MToken.MSymbol = currentRule.m_nonTerminal;
							head.MToken.MText = null;
							_rhsCount = currentRule.m_symbols.Length;

							parseResult = TokenParseResult.ReduceNormal;
							//========== Goto
							int k = _stackIndex - _rhsCount;
							nextState = _stack[k].MState;
						#if INCLUDE_TRIM_CODE
						}
						#endif
						//========= If nextAction is null here, then we have an Internal Table Error!!!!
						LrStateAction nextAction = nextState.m_transitionVector[currentRule.m_nonTerminal.m_index];

						if (nextAction != null)
						{
							CurrentLrState = _mGrammar.m_lrStateTable[nextAction.m_value];
							head.MState = CurrentLrState;
							if (parseResult == TokenParseResult.ReduceNormal)
							{
								_stackIndex++;
								if (_stack.Length == _stackIndex)
								{
									return TokenParseResult.StackError;
									//int newSize = stack.Length + MinimumLrStackSize;
									//LrStackItem[] largerMLrStack = new LrStackItem[newSize];
									//Array.Copy(stack, largerMLrStack, stack.Length);
									//stack = largerMLrStack;
									//Sc.ConWriteLine(MODE_STACK_GROW, "Parser.TokenParseResult() Grow Stack " + stack.Length);
								}
								_stack[_stackIndex] = head;
							}
							else
							{
								_stack[_stackIndex] = head;
							}
							//Sc.ConWriteLine(COLOR_REDUCE, "reduce to state " + CurrentLrState.m_index);
							Sc.ConWrite(COLOR_REDUCE, "reduce state = " + CurrentLrState.m_index);
							Sc.ConWriteLine(141, ", rule = " +  ruleIndex + " " + RuleText(ruleIndex));
//							Sc.ConWriteLine(141, ", rule = " + _mGrammar.m_ruleTable[ruleIndex].Tree());

							return parseResult;
						}
						else
							return TokenParseResult.InternalError;
				}
			}
			return TokenParseResult.SyntaxError;
		}


		private string RuleText(int ruleIndex)
		{
			StringBuilder sb = new StringBuilder();

			int lhsSymid = _mGrammar.m_ruleTable[ruleIndex].m_nonTerminal.Index;
			string lhs = "<" + _mGrammar.SymbolTexts[lhsSymid] + ">";
			sb.Append(lhs + " ::= ");

			for (int k = 0; k < _mGrammar.m_ruleTable[ruleIndex].m_symbols.Length; k++)
			{
				int symid = _mGrammar.m_ruleTable[ruleIndex].m_symbols[k].Index;
				string name = _mGrammar.SymbolTexts[symid];
				if (_mGrammar.m_ruleTable[ruleIndex].m_symbols[k].m_symbolType == SymbolType.NonTerminal)
					sb.Append("<" + name + "> ");
				else
					sb.Append(name + " ");
			}
			return sb.ToString();
		}

		private enum TokenParseResult
		{
			// Empty            = 0,
			Accept           = 1,
			Shift            = 2,
			ReduceNormal     = 3,
			//ReduceEliminated = 4, // don't need because we're not doing TrimReductions yet.
			SyntaxError      = 5,
			InternalError    = 6,
			StackError       = 7
		}

		private struct Token
		{
			internal Symbol MSymbol;     // Token symbol.
			internal string MText;       // Token text.
			internal int MStart;         // Token start stream start.
			internal int MLength;        // Token length.
			internal object MSyntaxNode; // Syntax node which can be attached to the token.
		}
		
		private struct LrStackItem
		{
			//∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎
			internal int Counter;    // Monotonically increasing counter per parse just to show what's happening in stack printouts.
			//∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎

			internal Token MToken;   // Token in the LR stack item.
			internal LrState MState; // LR state associated with the item.
			internal Rule MRule;     // Reference to a grammar rule if the item contains non-terminal.

			//∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎
			// This property is just so I can assign a value (-1) to Counter so I can filter printouts and not see the whole stack.
			public static implicit operator LrStackItem(int counter)
			{
				return new LrStackItem() { Counter = counter };
			}
			//∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎

			public override string ToString()
			{
				StringBuilder sb = new StringBuilder();
				//∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎
				sb.Append("CTR " + Counter.ToString("D2") + " ");
				//∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎
				sb.Append("Token(text=" + MToken.MText + " ");
				// sb.Append("start=" + MToken.MStart + " ");
				// sb.Append("length=" + MToken.MLength + " ");

				if (MToken.MSymbol != null)
					sb.Append("symbolid=" + MToken.MSymbol.Index + " ");
				else
					sb.Append("symbolid=null ");

				if (MState != null)
					sb.Append("lalrstate=" + MState.m_index + " ");

				if (MRule != null)
					sb.Append("rule=" + MRule.Index + " ");
				return sb.ToString();
			}
		}
	}
}

