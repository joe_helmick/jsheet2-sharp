#region Copyright

//----------------------------------------------------------------------
// Gold Parser engine.
// See more details on http://www.devincook.com/goldparser/
// 
// Original code is written in VB by Devin Cook (GOLDParser@DevinCook.com)
//
// This translation is done by Vladimir Morozov (vmoroz@hotmail.com)
// 
// The translation is based on the other engine translations:
// Delphi engine by Alexandre Rai (riccio@gmx.at)
// C# engine by Marcus Klimstra (klimstra@home.nl)
//----------------------------------------------------------------------

#endregion

namespace ConsoleApp1
{
	public class DfaState
	{
		public int m_index;
		internal Symbol m_acceptSymbol;
		internal ObjectMap m_transitionVector; 

		public DfaState(int index, Symbol acceptSymbol, ObjectMap transitionVector)
		{
			m_index = index;
			m_acceptSymbol = acceptSymbol;
			m_transitionVector = transitionVector;
		}
	}
}
