#region Copyright

//----------------------------------------------------------------------
// Gold Parser engine.
// See more details on http://www.devincook.com/goldparser/
// 
// Original code is written in VB by Devin Cook (GOLDParser@DevinCook.com)
//
// This translation is done by Vladimir Morozov (vmoroz@hotmail.com)
// 
// The translation is based on the other engine translations:
// Delphi engine by Alexandre Rai (riccio@gmx.at)
// C# engine by Marcus Klimstra (klimstra@home.nl)
//----------------------------------------------------------------------

#endregion

namespace ConsoleApp1
{
	public class LrState
	{
		public int m_index;
		private LrStateAction[] _actions;
		internal LrStateAction[] m_transitionVector;

		public LrState(int index, LrStateAction[] actions, LrStateAction[] transitionVector)
		{
			m_index = index;
			_actions = actions;
			m_transitionVector = transitionVector;
		}
	}
}
