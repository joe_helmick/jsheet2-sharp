#region Copyright

//----------------------------------------------------------------------
// Gold Parser engine.
// See more details on http://www.devincook.com/goldparser/
// 
// Original code is written in VB by Devin Cook (GOLDParser@DevinCook.com)
//
// This translation is done by Vladimir Morozov (vmoroz@hotmail.com)
// 
// The translation is based on the other engine translations:
// Delphi engine by Alexandre Rai (riccio@gmx.at)
// C# engine by Marcus Klimstra (klimstra@home.nl)
//----------------------------------------------------------------------

#endregion

#region Using directives

using System;

#endregion

namespace ConsoleApp1
{ 
	/// <summary>
	/// Maps integer values used for transition vectors to objects.
	/// </summary>
public class ObjectMap
	{
		#region Fields

		private bool _readonly;
		private MapProvider _mapProvider;

		private const int MAXINDEX = 255;
		private const int GROWTH = 32;
		private const int MINSIZE = 32;
		private const int MAXARRAYCOUNT = 12;

		private const int INVALIDKEY = Int32.MaxValue;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates new instance of <see cref="ObjectMap"/> class.
		/// </summary>
		public ObjectMap()
		{
			_mapProvider = new SortedMapProvider(MINSIZE);
		}

		#endregion

		#region Public members.

		/// <summary>
		/// Gets number of entries in the map.
		/// </summary>
		public int Count
		{
			get	{ return _mapProvider.m_count; }
		}

		/// <summary>
		/// Gets or sets read only flag.
		/// </summary>
		public bool ReadOnly 
		{
			get { return _readonly; }
			set 
			{ 
				if (_readonly != value)
				{
					SetMapProvider(value);
					_readonly = value; 
				}
			}
		}

		/// <summary>
		/// Gets or sets value by key.
		/// </summary>
		public object this[int key]
		{
			get { return _mapProvider[key]; }
			set { _mapProvider.Add(key, value); }
		}

		/// <summary>
		/// Returns key by index.
		/// </summary>
		/// <param name="index">Zero based index of the requested key.</param>
		/// <returns>Returns key for the given index.</returns>
		public int GetKey(int index)
		{
			return _mapProvider.GetEntry(index).Key;
		}

		/// <summary>
		/// Removes entry by its key.
		/// </summary>
		/// <param name="key"></param>
		public void Remove(int key)
		{
			_mapProvider.Remove(key);
		}

		/// <summary>
		/// Adds a new key and value pair. 
		/// If key exists then value is applied to existing key.
		/// </summary>
		/// <param name="key">New key to add.</param>
		/// <param name="value">Value for the key.</param>
		public void Add(int key, object value)
		{
			_mapProvider.Add(key, value);
		}

		#endregion

		#region Private members

		private void SetMapProvider(bool readOnly)
		{
			int count = _mapProvider.m_count;
			MapProvider provider = _mapProvider;
			if (readOnly)
			{
				SortedMapProvider pr = _mapProvider as SortedMapProvider;
				if (pr.m_lastKey <= MAXINDEX)
				{
					provider = new IndexMapProvider();
				}
				else if (count <= MAXARRAYCOUNT)
				{
					provider = new ArrayMapProvider(_mapProvider.m_count);
				}
			}
			else
			{
				if (! (provider is SortedMapProvider))
				{
					provider = new SortedMapProvider(_mapProvider.m_count);
				}
			}
			if (provider != _mapProvider)
			{
				for (int i = 0; i < count; i++)
				{
					Entry entry = _mapProvider.GetEntry(i);
					provider.Add(entry.Key, entry.Value);
				}
				_mapProvider = provider;
			}
		}

		#endregion

		#region Entry struct definition

		private struct Entry
		{
			internal int Key;
			internal object Value;

			internal Entry(int key, object value)
			{
				Key = key;
				Value = value;
			}
		}

		#endregion

		private abstract class MapProvider 
		{
			internal int m_count;        // Entry count in the collection.

			internal abstract object this[int key]
			{
				get;
			}

			internal abstract Entry GetEntry(int index);

			internal abstract void Add(int key, object value);

			internal virtual void Remove(int key)
			{
				throw new InvalidOperationException();
			}
		}

		private class SortedMapProvider : MapProvider
		{
			internal Entry[] m_entries; // Array of entries.

			internal int m_lastKey;      // Bigest key number.

			internal SortedMapProvider(int capacity)
			{
				m_entries = new Entry[capacity];
			}

			internal override object this[int key]
			{
				get 
				{
					int minIndex = 0;
					int maxIndex = m_count - 1;
					if (maxIndex >= 0 && key <= m_lastKey)
					{
						do
						{
							int midIndex = (maxIndex + minIndex) / 2;
							if (key <= m_entries[midIndex].Key)
							{
								maxIndex = midIndex;
							}
							else
							{
								minIndex = midIndex + 1;
							}
						} while (minIndex < maxIndex);
						if (key == m_entries[minIndex].Key)
						{
							return m_entries[minIndex].Value;
						}
					}
					return null;
				}
			}

			internal override Entry GetEntry(int index)
			{
				return m_entries[index];
			}

			internal override void Add(int key, object value)
			{
				bool found;
				int index = FindInsertIndex(key, out found);
				if (found)
				{
					m_entries[index].Value = value;
					return;
				}
				if (m_count >= m_entries.Length)
				{
					Entry[] entries = new Entry[m_entries.Length + GROWTH];
					Array.Copy(m_entries, 0, entries, 0, m_entries.Length);
					m_entries = entries;
				}
				if (index < m_count)
				{
					Array.Copy(m_entries, index, m_entries, index + 1, m_count - index);
				}
				else
				{
					m_lastKey = key;
				}
				m_entries[index].Key = key;
				m_entries[index].Value = value;
				m_count++;
			}

			private int FindInsertIndex(int key, out bool found)
			{
				int minIndex = 0;
				if (m_count > 0 && key <= m_lastKey)
				{
					int maxIndex = m_count - 1;
					do
					{
						int midIndex = (maxIndex + minIndex) / 2;
						if (key <= m_entries[midIndex].Key)
						{
							maxIndex = midIndex;
						}
						else
						{
							minIndex = midIndex + 1;
						}
					} while (minIndex < maxIndex);
					found = (key == m_entries[minIndex].Key);
					return minIndex;
				}
				found = false;
				return m_count;
			}
		}

		private class IndexMapProvider : MapProvider
		{
			private object[] _array; // Array of entries.			

			internal IndexMapProvider()
			{
				_array = new object[MAXINDEX + 1];
				for (int i = _array.Length; --i >= 0; )
				{
					_array[i] = Unassigned.Value;
				}
			}

			internal override object this[int key]
			{
				get 
				{ 
					if (key >= _array.Length || key < 0)
					{
						return null;
					}
					return _array[key]; 
				}
			}

			internal override Entry GetEntry(int index)
			{
				int idx = -1;
				for (int i = 0; i < _array.Length; i++)
				{
					object value = _array[i];
					if (value != Unassigned.Value)
					{
						idx++;
					}
					if (idx == index)
					{
						return new Entry(i, value);
					}
				}
				return new Entry();
			}

			internal override void Add(int key, object value)
			{
				_array[key] = value;
				m_count++;
			}
		}
		
		private class ArrayMapProvider : MapProvider
		{
			private Entry[] _entries; // Array of entries.			

			internal ArrayMapProvider(int capacity)
			{
				_entries = new Entry[capacity];
			}

			internal override object this[int key]
			{
				get 
				{ 
					for (int i = m_count; --i >= 0;)
					{
						Entry entry = _entries[i];
						int entryKey = entry.Key;
						if (entryKey > key)
						{
							continue;
						}
						else if (entryKey == key)
						{
							return entry.Value;
						}
						else if (entryKey < key)
						{
							return null;
						}
					}
					return null; 
				}
			}

			internal override Entry GetEntry(int index)
			{
				return _entries[index];
			}

			internal override void Add(int key, object value)
			{
				_entries[m_count].Key = key;
				_entries[m_count].Value = value;
				m_count++;
			}
		}

		private class Unassigned
		{
			internal static Unassigned Value = new Unassigned();
		}
	}
}
