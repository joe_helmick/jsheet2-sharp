﻿namespace ConsoleApp1
{
	public enum SymbolConstants
	{
		// Special
		Eof = 0,
		Error = 1,
		// Whitespace
		Space = 2,
		// Terminals
		Minus = 3,
		LParen = 4,
		RParen = 5,
		Mult = 6,
		Div = 7,
		Plus = 8,
		CellRef = 9,
		Number = 10,
		// NonTerminals excluded.
	}

	public enum RuleConstants
	{
		FormulaAdd = 0, // comment out if TrimReductions = true
		Formula = 1,
		AddAddPlusMult = 2,
		AddAddMinusMult = 3,
		AddMult = 4, // comment out if TrimReductions = true
		MultMultTimesNeg = 5,
		MultMultDivNeg = 6,
		MultNeg = 7,  // comment out if TrimReductions = true
		NegMinusValue = 8,
		NegValue = 9, // comment out if TrimReductions = true
		ValueCellRef = 10,
		ValueNumber = 11,
		ValueParenExp = 12,
	}


	public enum LrAction
	{
		//None = 0,
		Shift = 1,
		Reduce = 2,
		//Goto = 3,
		Accept = 4,
		//Error = 5
	}

	public enum ParseMessage
	{
		//Empty = 0,
		TokenRead = 1,
		Reduction = 2,
		Accept = 3,
		//NotLoadedError = 4,
		LexicalError = 5,
		SyntaxError = 6,
		//CommentError = 7,
		InternalError = 8,
		//CommentBlockRead = 9,
		//CommentLineRead = 10,
	}

	public enum SymbolType
	{
		NonTerminal = 0,
		Terminal = 1,
		WhiteSpace = 2,
		End = 3,
		//CommentStart = 4,
		//CommentEnd = 5,
		//CommentLine = 6,
		Error = 7
	}
}
