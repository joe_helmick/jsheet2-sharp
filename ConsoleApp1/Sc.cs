﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
	public static class Sc
	{
		public const string GRAMMAR_FILE_NAME = @"D:\dev\Z80-ROMs\GoldParser01\jsheet3.cgt";
		public static Grammar Grammar;
		public static readonly List<KeyValuePair<string, string>> FormulasList = new List<KeyValuePair<string, string>>();
		public static bool ParseSuccess;
		public static bool FindingDependencies { get; set; }
		public static Dictionary<string, int> CellRefs = new Dictionary<string, int>();
		public static Dictionary<string, List<string>> Dependencies = new Dictionary<string, List<string>>();
		public static string ErrorText;
		public static bool UnrecoverableErrorFound;
		public static List<string> TopoOrdered;
		public static List<string> StartSet;
		public static Dictionary<string, int> NodeState;
		public const int TOPOSORT_GRAY = 0;
		public const int TOPOSORT_BLACK = 1;
		public static int StateK;
		public static Expression Formula;
		public static string ThisFormula;
		public static string ThisCellReference;
		public static int StackCounter = 0;

		public static void ConWriteLine(int mode, string message)
		{
			Console.Write("\x1b[38;5;" + mode + "m");
			Console.WriteLine(message);
			Console.Write("\x1b[38;5;" + 255 + "m");
		}

		public static void ConWrite(int mode, string message)
		{
			Console.Write("\x1b[38;5;" + mode + "m");
			Console.Write(message);
			Console.Write("\x1b[38;5;" + 255 + "m");
		}
	}
}
